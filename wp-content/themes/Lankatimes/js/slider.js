

/////////////////////////////////this JavaScript class belongs to HotelFounder small slider animation///////////////////////

/**
 *
 * @author Thilina Madusanka
 */

///// Hotel Founder Image Slider variables(Globle use)/////////////////////
var hotelFoundersmallSliderUl;
var hotelFounderListItems;
var hotelFounderImageNumber;
var hotelFounderSliderImageWidth;
var hotelFounder_previousButton, hotelFounder_nextButton;
var sliderCurrentPosition = 0;
var sliderCurrentImage = 0;

//	when click previousButton ,call onClickPrev(); method////
//	when click nextButton ,call hotelFounder_previousButton(); method////


function init(){
	
	hotelFoundersmallSliderUl = document.getElementById('hotelFounder_image_slider');
	hotelFounderListItems = hotelFoundersmallSliderUl.children;
	hotelFounderImageNumber = hotelFounderListItems.length;
	hotelFounderSliderImageWidth = hotelFounderListItems[0].children[0].clientWidth;
	hotelFoundersmallSliderUl.style.width = parseInt(hotelFounderSliderImageWidth * hotelFounderImageNumber) + 'px';
	hotelFounder_previousButton = document.getElementById("infinity_lanka_sliderPreviousButton");
	hotelFounder_nextButton = document.getElementById("infinity_lanka_sliderNextButton");

	hotelFounder_previousButton.onclick = function(){ 
		onClickPrev();
	};
	hotelFounder_nextButton.onclick = function(){ 
		onClickNext();
	};
}

///////this is animation method////
//  @param 'opts'
// return id

function animate(opts){
	var start = new Date;
	var id = setInterval(function(){
		var timePassed = new Date - start;
		var progress = timePassed / opts.duration;
		if (progress > 1){
			progress = 1;
		}
		var delta = opts.delta(progress);
		opts.step(delta);
		if (progress == 1){
			clearInterval(id);
			opts.callback();
		}
	}, opts.delay || 17);
	
}

///////this is animation method////
//  @param 'imageToGo'
//

function slideTo(imageToGo){
	var direction;
	var numOfImageToGo = Math.abs(imageToGo - sliderCurrentImage);
	////// slide toward left

	direction = sliderCurrentImage > imageToGo ? 1 : -1;
	sliderCurrentPosition = -1 * sliderCurrentImage * hotelFounderSliderImageWidth;
	var opts = {
		duration:1000,
		delta:function(p){return p;
		},
		step:function(delta){
			hotelFoundersmallSliderUl.style.left = parseInt(sliderCurrentPosition + direction * delta * hotelFounderSliderImageWidth * numOfImageToGo) + 'px';
		},
		callback:function(){sliderCurrentImage = imageToGo;
		}	
	};
	animate(opts);
}
   //// this is image toward previous
function onClickPrev(){
	if (sliderCurrentImage == 0){
		slideTo(hotelFounderImageNumber - 1);
	} 		
	else{
		slideTo(sliderCurrentImage - 1);
	}		
}

function onClickNext(){
	if (sliderCurrentImage == hotelFounderImageNumber - 1){
		slideTo(0);
	}		
	else{
		slideTo(sliderCurrentImage + 1);
	}		
}

window.onload = init;

