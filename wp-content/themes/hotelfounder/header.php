<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">
		<script src="https://code.jquery.com/jquery.js"></script>

		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

	</head>
	<body <?php body_class(); ?>>

		 <!--##################################### This is Hotel Founder top Header######################################-->
  <!--############################################################################################################-->

  <!-- /header -->
  <header id="header" >
    <div class="reg"  >
      <a href="#">Login</a> | <a href="#">Register</a>
    </div><hr >  
  </header>
  <!--############################################# Ending top Header######################################-->
  <!--#######################################################################################################-->










   <!--################################## This is HotelFounder Logo Image Container###############################-->
     <!--######################################################################################################-->
  <div class="container" id="hotelFounder_headerContainer">
     <div class="hotelFounder_LogoImages_OtherImages">
      <div class="row" >
      <div class="col-md-5" id="hotelFounder_columRow">
      <div  id="hotelFounder_headerContainerColum">
        <img class="hotel_Founder_Logo" src="<?php echo get_template_directory_uri(); ?>/img/hotel-founder-logo.png" class="logo "></img>
      </div> 
      </div> 
      <div class="col-md-7" class="hotelFounder_otherImages">
        <div class="row" >
        <div class="col-md-8" id="hotelFounder_ulList">
        
        <ul class="hotelFounder_headerUl">
        <li class="hotelFounder_phoneImage" ><b id="hotelFounder_phoneNumberText" id="hotelFounder_headerText" class="hotelFounder_headerText_">+94 777 125 126</b></li>            
        <li class="hotelFounder_emailImageIcon"><b id="hotelFounder_emailText" id="hotelFounder_headerText" class="hotelFounder_headerText__" >info@hotelfounder.lk</b></li>
        </ul>
      
      </div>
      
      <div class="col-md-3" id="hotelFounder_socialSpriteImage">
        <div class="hotelFounder_socialIcons">
        <a class="hotelFounder_googleIcon"></a>
        <a class="hotelFounder_twitterIcon"></a>
        <a class="hotelFounder_facebookIcon"></a>
        <a class="hotelFounder_feedIcon" ></a>
        </div>
      </div>
       </div>  
      </div>  
      </div>
    </div>
    </div>
  </div>


<!--############################## This is End of HotelFounder Logo Image Container#############################-->
   <!--######################################################################################################-->


   <!--################################## This is HotelFounder  Navigation Bar##################################-->
     <!--######################################################################################################-->

     <nav class="navbar navbar-inverse " role="navigation" >
       <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" >
                    <span class="sr-only"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"></a>
            </div>
            
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2" >
                <ul class="nav navbar-nav">
                    <li class="active" >
                        <a class="home"  id="homebutton" href="file:///C:/Users/HP/Desktop/HotelFounder/Home.html" color="blue"><img src="<?php echo get_template_directory_uri(); ?>/img/home.png" alt=""></a>
                    </li>
                    <li>
                        <a class="about" href="file:///C:/Users/HP/Desktop/HotelFounder/AboutUs.html" color="blue">About Us</a>
                    </li>
                    <li>
                        <a class="booking" href="#">Hotel Booking</a>
                    </li>
                    <li>
                        <a class="tour" href="#">Tour Planing</a>
                    </li>
                    <li>
                        <a class="services" href="#">Services</a>
                    </li>
                    <li>
                        <a class="gallery" href="#">Gallery</a>
                    </li>
                    <li>
                        <a class="testimonial" href="#">Testimonial</a>
                    </li>
                    <li>
                        <a class="arrival" href="#">New Arrival</a>
                    </li>
                </ul>
            </div>
           
        </div>
         </div>
       
    </nav>

  <!--################################### This is End of Navigaton Bar #######################################-->
   <!--######################################################################################################-->



                  
                   <!--   <a class="logo " href="<?php echo home_url(); ?>"> -->
                     
                 <!--  <?php html5blank_nav();  ?>  -->

			</header>
			<!-- /header -->
