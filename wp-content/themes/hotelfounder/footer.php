			<!-- footer -->
			<footer class="footer" role="contentinfo">

				   <!--########################################################################################################-->

<div class="HotelFounder_Mainfotter">
  <div class="container">
    <div class="row" id="fonts">
      <div class="col-md-1" id="hotelFounder_quickLinksColum">
          <p id="whiteFont" class="hotelFounder_fotterQuickLinks">Quick Links</p><br>
            <li class="list-unstyled">
              <a class="hotelFounder_aboutUs" href="#" >About Us</a>
            </li>
            <li class="list-unstyled">
              <a class="hotelFounder_booking" href="#">Hotel Booking</a>
            </li>
            <li class="list-unstyled">
              <a class="hotelFounder_tourPlanng" href="#">Tour Planing</a>
            </li>
            <li class="list-unstyled">
              <a class="hotelFounder_services" href="#">Services</a>
            </li >
            <li class="list-unstyled">
              <a class="hotelFounder_Gallery" href="#">Gallery</a>
            </li>
            <li class="list-unstyled">
              <a class="hotelFounder_testimonial" href="#">Testimonial</a>
            </li>
            <li class="list-unstyled">
              <a class="hotelFounder_arrival" href="#">New Arrival</a>
            </li>
          </div>
         

  <div class="col-md-3" id="hotelFounder_aboutUsColum" > 
      <p id="whiteFont" class="hotelFounder_describeAboutUs">About Us</p><br>
        <p style="margin-top: 0;"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tincidunt ante eget mi pharetra, vehicula fermentum diam ultricies. Sed porttitor, ligula sit amet varius tincidunt, lacus metus suscipit risus, eu pellentesque ipsum quam congue purus. Sed ornare vestibulum molestie.</p>
      </div >
  
  <div class="col-md-3" id="hotelFounder_contactUsColum"> 
   <div>
      <p class="hotelFounder_describecontactUs" id="whiteFont" >Contact Us</p><br>
</div>
      <p style="margin-top: 0;">  <img src="./img/phone-gray.png"></img> +94 777 125 126 / +94 777 114 115 </p><br>
        <p><img src="./img/mail-gray.png"> </img> info@hotelfounder.lk</p><br>
        <p><img src="./img/location-gray.png"></img> 350 Fifth Avenue, 34th floor,</p> <br>
   
</div>
<div class="col-md-3" id="hotelFounder_newsLetterColum" style="padding: 0;"> 
  
    <p class="hotelFounder_newsLetters" id="whiteFont"  >NewsLetters</p><br>
     <p style="margin-top: 0;"> Signup for the monthly newsletter to get usefull information for your business</p><br><br>
      <input  id="hotelFounder_signUpField" name="format" type="text"  > 
      <button type="submit"   id="hotelFounder_signUpButton">Sign Up</button>
     
</div>
<div class="col-md-2" id="hotelFounder_socialMediaColum">
  <div >
    <p class="hotelFounder_newsLetters" id="whiteFont">Social Media</p><br>
      <div class="hotelFounder_socialIconsSecond">
        <a class="hotelFounder_googleIconGray"></a>
        <a class="hotelFounder_twitterIconGray"></a>
        <a class="hotelFounder_facebookIconGray"></a>
        <a class="hotelFounder_feedIconGray" ></a>
        </div><br><br><br><br><br>
        <div class="HotelFounderLogo">
      <img src="<?php echo get_template_directory_uri(); ?>/img/logo-white-small.png">
      </div>
    </div> 
  </div>
</div>
</div>
</div>




				<!-- copyright -->
				
				<!--	&copy; <?php echo date('Y'); ?> Copyright <?php bloginfo('name'); ?>. <?php _e('Powered by', 'html5blank'); ?>
					<a href="//wordpress.org" title="WordPress">WordPress</a> &amp; <a href="//html5blank.com" title="HTML5 Blank">HTML5 Blank</a>. -->
				
				<!-- /copyright -->

			
			<!-- /footer -->
		
			<!-- /container -->
		
		<!-- /container full width -->

		<?php wp_footer(); ?>

		<!-- analytics -->
		<script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
		ga('send', 'pageview');
		</script>

	</body>
</html>
